<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RaceTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('racetypes')->insert(array(
         		[	
         			'id' => 1,
		            'name' => 'Thoroughbred',
		            'created_at' => Carbon::now(),
	            ],
	            [	
	            	'id' => 2,
		            'name' => 'Greyhound',
		            'created_at' => Carbon::now(),
	            ],
	            [	
	            	'id' => 3,
		            'name' => 'Harness',
		            'created_at' => Carbon::now(),
	            ]
         	)
         	
         );
    }
}
