<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class RacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('races')->insert(array(
         		[	
         			
		            'name' => 'R1',
		            'starting_time' => Carbon::now('Australia/Sydney')->addMinutes(50),
		            'close_time' => '00:50:00',
		            'meeting_id' => 1,
		            'created_at' => Carbon::now('Australia/Sydney'),
	            ],
	            [	
	            	
		            'name' => 'R2',
		            'starting_time' => Carbon::now('Australia/Sydney')->addMinutes(38),
		            'close_time' => '00:38:00',
		            'meeting_id' => 1,
		            'created_at' => Carbon::now('Australia/Sydney'),
	            ],
	            [	
	            	
		            'name' => 'R3',
		            'starting_time' => Carbon::now('Australia/Sydney')->addMinutes(120),
		            'close_time' => '02:00:00',
		            'meeting_id' => 1,
		            'created_at' => Carbon::now('Australia/Sydney'),
	            ],
	            [	
	            	
		            'name' => 'R4',
		            'starting_time' => Carbon::now('Australia/Sydney')->addMinutes(150),
		            'close_time' => '02:30:00',
		            'meeting_id' => 1,
		            'created_at' => Carbon::now('Australia/Sydney'),
	            ],
	            [	
	            	
		            'name' => 'R5',
		            'starting_time' => Carbon::now('Australia/Sydney')->addMinutes(200),
		            'close_time' => '03:20:00',
		            'meeting_id' => 1,
		            'created_at' => Carbon::now('Australia/Sydney'),
	            ],
	            [	
         			
		            'name' => 'R1',
		            'starting_time' => Carbon::now('Australia/Sydney')->addMinutes(50),
		            'close_time' => '00:50:00',
		            'meeting_id' => 2,
		            'created_at' => Carbon::now('Australia/Sydney'),
	            ],
	            [	
	            	
		            'name' => 'R2',
		            'starting_time' => Carbon::now('Australia/Sydney')->addMinutes(80),
		            'close_time' => '01:20:00',
		            'meeting_id' => 2,
		            'created_at' => Carbon::now('Australia/Sydney'),
	            ],
	            [	
	            	
		            'name' => 'R3',
		            'starting_time' => Carbon::now('Australia/Sydney')->addMinutes(120),
		            'close_time' => '02:00:00',
		            'meeting_id' => 2,
		            'created_at' => Carbon::now('Australia/Sydney'),
	            ],
	            [	
	            	
		            'name' => 'R4',
		            'starting_time' => Carbon::now('Australia/Sydney')->addMinutes(150),
		            'close_time' => '03:00:00',
		            'meeting_id' => 2,
		            'created_at' => Carbon::now('Australia/Sydney'),
	            ],
	            [	
	            	
		            'name' => 'R5',
		            'starting_time' => Carbon::now('Australia/Sydney')->addMinutes(400),
		            'close_time' => '06:40:00',
		            'meeting_id' => 2,
		            'created_at' => Carbon::now('Australia/Sydney'),
	            ],
	            [	
         			
		            'name' => 'R1',
		            'starting_time' => Carbon::now('Australia/Sydney')->addMinutes(250),
		            'close_time' => '04:10:00',
		            'meeting_id' => 3,
		            'created_at' => Carbon::now('Australia/Sydney'),
	            ],
	            [	
	            	
		            'name' => 'R2',
		            'starting_time' => Carbon::now('Australia/Sydney')->addMinutes(300),
		            'close_time' => '05:00:00',
		            'meeting_id' => 3,
		            'created_at' => Carbon::now('Australia/Sydney'),
	            ],
	            [	
	            	
		            'name' => 'R3',
		            'starting_time' => Carbon::now('Australia/Sydney')->addMinutes(80),
		            'close_time' => '01:20:00',
		            'meeting_id' => 3,
		            'created_at' => Carbon::now('Australia/Sydney'),
	            ],
	            [	
	            	
		            'name' => 'R4',
		            'starting_time' => Carbon::now('Australia/Sydney')->addMinutes(80),
		            'close_time' => '01:20:00',
		            'meeting_id' => 3,
		            'created_at' => Carbon::now('Australia/Sydney'),
	            ],
	            [	
	            	
		            'name' => 'R5',
		            'starting_time' => Carbon::now('Australia/Sydney')->addMinutes(20),
		            'close_time' => '00:20:00',
		            'meeting_id' => 3,
		            'created_at' => Carbon::now('Australia/Sydney'),
	            ]
         	)
         	
         );
    }
}
