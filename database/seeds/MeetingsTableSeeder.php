<?php

use Illuminate\Database\Seeder;

class MeetingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('meetings')->insert(array(
        		[
	            	'location' => 'Albion Park',
	            	'racetype_id' => '3',
	            ],
	            [
	            	'location' => 'Ascot Park',
	            	'racetype_id' => '3',
	            ],
	            [
	            	'location' => 'Bunbury',
	            	'racetype_id' => '3',
	            ],
	            [
	            	'location' => 'Cranbourne',
	            	'racetype_id' => '3',
	            ],	          
	            [
	            	'location' => 'Menangle',
	            	'racetype_id' => '3',
	            ],
	            [
	            	'location' => 'Newcastle',
	            	'racetype_id' => '3',
	            ],
	           	[
	            	'location' => 'Bulli',
	            	'racetype_id' => '2',
	            ],
	            [
	            	'location' => 'Cannington',
	            	'racetype_id' => '2',
	            ],
	            [
	            	'location' => 'Horsham',
	            	'racetype_id' => '2',
	            ],
	            [
	            	'location' => 'Ipswich',
	            	'racetype_id' => '2',
	            ],
	            [
	            	'location' => 'The Gardens',
	            	'racetype_id' => '2',
	            ],
	            [
	            	'location' => 'The Meadows',
	            	'racetype_id' => '2',
	            ],
	            [
	            	'location' => 'Wentworth Park',
	            	'racetype_id' => '2',
	            ],
	            [
	            	'location' => 'Belmont',
	            	'racetype_id' => '1',
	            ],
	            [
	            	'location' => 'Bowraville',
	            	'racetype_id' => '1',
	            ],
	            [
	            	'location' => 'Caulfield',
	            	'racetype_id' => '1',
	            ],
	            [
	            	'location' => 'Darwin',
	            	'racetype_id' => '1',
	            ],
	            [
	            	'location' => 'Gatton',
	            	'racetype_id' => '1',
	            ],
	            [
	            	'location' => 'Hastings',
	            	'racetype_id' => '1',
	            ],
	            [
	            	'location' => 'Morphettville',
	            	'racetype_id' => '1',
	            ],
        	)
        	

        );
    }
}
