# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Racing data API

### How do I get set up? ###

1. Change .env.example file to .env , and set up database name and password.

2. Run `composer intall` to install all dependencies.

3. Run `php artisan migrate` to migrate all tables in database.

4. Run `php artisan db:seed` to seed sample data into database.

5. Set virtual host as racing.dev

