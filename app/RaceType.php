<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RaceType extends Model
{
  	protected $table = "racetypes";

    public function meetings()
    {
        return $this->hasMany('App\Meeting');
    }
}
