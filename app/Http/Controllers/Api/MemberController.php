<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Member;

class MemberController extends Controller
{
     /**
     * return member for api.
     *
     * @param  $request
     * @return $member
     */
    protected function getmember(Request $request)
    {   
        //get api url parameters and limit to only firstname, surname and email
        $query_paras = $request->only('firstname','surname','email');

         //no parameters from url show all members
        if (is_null($query_paras['firstname']) && is_null($query_paras['surname']) && is_null($query_paras['email'])) {
            //get searched result
            $searched_members = Member::all();

        }else{
             //check if url parameter contains firstname
            if (!is_null($query_paras['firstname'])) {
                
                $query_arr['firstname'] = $query_paras['firstname'];
            }
            //check if url parameters contains surname
            if (!is_null($query_paras['surname'])) {
                
                $query_arr['surname'] = $query_paras['surname'];
            }
            //check if url parameters contains email
            if (!is_null($query_paras['email'])) {
                
                $query_arr['email'] = $query_paras['email'];
            }  
            
            //get search result
            $searched_members = Member::where($query_arr)->get();
        }
             
        return $searched_members;
    }
}
